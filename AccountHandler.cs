using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace FediCleaner {
    public class AccountHandler {
        private string Username;
        private string Credentials;
        private string Site;

        public AccountHandler(Settings Configuration) {
            Console.Write($"Input your site url (ie. domain aka shitposter.club) [{Configuration.Account.Site}]: ");
            this.Site = GetInput(Configuration.Account.Site);
            Configuration.Account.Site = this.Site;
            Console.Write($"Your username [{Configuration.Account.Username}]: ");
            this.Username = GetInput(Configuration.Account.Username);
            Configuration.Account.Username = this.Username;
            Console.Write("And your password [this is not saved]: ");
            var pass = string.Empty;
            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && pass.Length > 0)
                {
                    Console.Write("\b \b");
                    pass = pass[0..^1];
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write("*");
                    pass += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);
            Console.WriteLine("");
            CreateCredentials(Configuration.Account.Username, pass);
        }

        private string GetInput(string Default) {
            test_loop:
            string Input = Console.ReadLine();
            Input = String.IsNullOrEmpty(Input) ? Default: Input;
            if (String.IsNullOrEmpty(Input)) {
                Console.Write("This value cannot be empty. Please enter a value: ");
                goto test_loop;
            }
            return Input;
        }


        public AccountHandler(string site, string username, string password) {
            this.Site = site;
            this.Username = username;
            CreateCredentials(username, password);
        }

        private void CreateCredentials(string username, string password) {
            this.Credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));
        }

        private AuthenticationHeaderValue AuthHeader() {
            return new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", this.Credentials);
        }
        public List<StatusList> GetStatuses(string max_id = null){
            using (HttpClient client = new HttpClient()) {
                var response = client.GetAsync($"https://{this.Site}/api/v1/accounts/{this.Username}/statuses?limit=400{(max_id == null ? "" : $"&max_id={max_id}")}");
                string data = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<List<StatusList>>(data);
            }
        }

        public List<StatusList> GetFavorites(string max_id = null){
            using (HttpClient client = new HttpClient()) {
                client.DefaultRequestHeaders.Authorization = AuthHeader();
                var response = client.GetAsync($"https://{this.Site}/api/v1/favourites?limit=400{(max_id == null ? "" : $"&max_id={max_id}")}");
                string data = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<List<StatusList>>(data);
            }
        }

        public void DeleteStatus(string id) {
            using (HttpClient client = new HttpClient()) {
                client.DefaultRequestHeaders.Authorization = AuthHeader();
                var response = client.SendAsync(new HttpRequestMessage{
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri($"https://{this.Site}/api/v1/statuses/{id}")
                });
                string data = response.Result.Content.ReadAsStringAsync().Result;
            }
        }

        public void DeleteReblog(string id) {
            using (HttpClient client = new HttpClient()) {
                client.DefaultRequestHeaders.Authorization = AuthHeader();
                var response = client.SendAsync(new HttpRequestMessage{
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"https://{this.Site}/api/v1/statuses/{id}/unreblog")
                });
                string data = response.Result.Content.ReadAsStringAsync().Result;
            }
        }

        public void DeleteFavorite(string id) {
            using (HttpClient client = new HttpClient()) {
                client.DefaultRequestHeaders.Authorization = AuthHeader();
                var response = client.SendAsync(new HttpRequestMessage{
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"https://{this.Site}/api/v1/statuses/{id}/unfavourite")
                });
                string data = response.Result.Content.ReadAsStringAsync().Result;
            }
        }
    }
}