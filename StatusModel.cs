
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
namespace FediCleaner
{

    public partial class StatusList
    {
        [JsonProperty("account")]
        public Account Account { get; set; }

        [JsonProperty("application")]
        public Application Application { get; set; }

        [JsonProperty("bookmarked")]
        public bool Bookmarked { get; set; }

        [JsonProperty("card")]
        public Card Card { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("emojis")]
        public Emoji[] Emojis { get; set; }

        [JsonProperty("favourited")]
        public bool Favourited { get; set; }

        [JsonProperty("favourites_count")]
        public long FavouritesCount { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("in_reply_to_account_id")]
        public string InReplyToAccountId { get; set; }

        [JsonProperty("in_reply_to_id")]
        public string InReplyToId { get; set; }

        [JsonProperty("language")]
        public object Language { get; set; }

        [JsonProperty("media_attachments")]
        public MediaAttachment[] MediaAttachments { get; set; }

        [JsonProperty("mentions")]
        public Mention[] Mentions { get; set; }

        [JsonProperty("muted")]
        public bool Muted { get; set; }

        [JsonProperty("pinned")]
        public bool Pinned { get; set; }

        [JsonProperty("pleroma")]
        public StatusListPleroma Pleroma { get; set; }

        [JsonProperty("poll")]
        public object Poll { get; set; }

        [JsonProperty("reblog")]
        public StatusList Reblog { get; set; }

        [JsonProperty("reblogged")]
        public bool Reblogged { get; set; }

        [JsonProperty("reblogs_count")]
        public long ReblogsCount { get; set; }

        [JsonProperty("replies_count")]
        public long RepliesCount { get; set; }

        [JsonProperty("sensitive")]
        public bool Sensitive { get; set; }

        [JsonProperty("spoiler_text")]
        public string SpoilerText { get; set; }

        [JsonProperty("tags")]
        public object[] Tags { get; set; }

        [JsonProperty("text")]
        public object Text { get; set; }

        [JsonProperty("uri")]
        public Uri Uri { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }

    public partial class Account
    {
        [JsonProperty("acct")]
        public string Acct { get; set; }

        [JsonProperty("avatar")]
        public Uri Avatar { get; set; }

        [JsonProperty("avatar_static")]
        public Uri AvatarStatic { get; set; }

        [JsonProperty("bot")]
        public bool Bot { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("emojis")]
        public object[] Emojis { get; set; }

        [JsonProperty("fields")]
        public Field[] Fields { get; set; }

        [JsonProperty("followers_count")]
        public long FollowersCount { get; set; }

        [JsonProperty("following_count")]
        public long FollowingCount { get; set; }

        [JsonProperty("fqn")]
        public string Fqn { get; set; }

        [JsonProperty("header")]
        public Uri Header { get; set; }

        [JsonProperty("header_static")]
        public Uri HeaderStatic { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("locked")]
        public bool Locked { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("pleroma")]
        public AccountPleroma Pleroma { get; set; }

        [JsonProperty("source")]
        public Source Source { get; set; }

        [JsonProperty("statuses_count")]
        public long StatusesCount { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }

    public partial class Field
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class AccountPleroma
    {
        [JsonProperty("accepts_chat_messages")]
        public bool? AcceptsChatMessages { get; set; }

        [JsonProperty("also_known_as")]
        public object[] AlsoKnownAs { get; set; }

        [JsonProperty("ap_id")]
        public Uri ApId { get; set; }

        [JsonProperty("background_image")]
        public object BackgroundImage { get; set; }

        [JsonProperty("favicon")]
        public object Favicon { get; set; }

        [JsonProperty("hide_favorites")]
        public bool HideFavorites { get; set; }

        [JsonProperty("hide_followers")]
        public bool HideFollowers { get; set; }

        [JsonProperty("hide_followers_count")]
        public bool HideFollowersCount { get; set; }

        [JsonProperty("hide_follows")]
        public bool HideFollows { get; set; }

        [JsonProperty("hide_follows_count")]
        public bool HideFollowsCount { get; set; }

        [JsonProperty("is_admin")]
        public bool IsAdmin { get; set; }

        [JsonProperty("is_confirmed")]
        public bool IsConfirmed { get; set; }

        [JsonProperty("is_moderator")]
        public bool IsModerator { get; set; }

        [JsonProperty("relationship")]
        public Relationship Relationship { get; set; }

        [JsonProperty("skip_thread_containment")]
        public bool SkipThreadContainment { get; set; }

        [JsonProperty("tags")]
        public object[] Tags { get; set; }
    }

    public partial class Relationship
    {
    }

    public partial class Source
    {
        [JsonProperty("fields")]
        public object[] Fields { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("pleroma")]
        public SourcePleroma Pleroma { get; set; }

        [JsonProperty("sensitive")]
        public bool Sensitive { get; set; }
    }

    public partial class SourcePleroma
    {
        [JsonProperty("actor_type")]
        public string ActorType { get; set; }

        [JsonProperty("discoverable")]
        public bool Discoverable { get; set; }
    }

    public partial class Application
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("website")]
        public Uri Website { get; set; }
    }

    public partial class Card
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("pleroma")]
        public CardPleroma Pleroma { get; set; }

        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [JsonProperty("provider_url")]
        public Uri ProviderUrl { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }
    }

    public partial class CardPleroma
    {
        [JsonProperty("opengraph")]
        public Dictionary<string, string> Opengraph { get; set; }
    }

    public partial class Emoji
    {
        [JsonProperty("shortcode")]
        public string Shortcode { get; set; }

        [JsonProperty("static_url")]
        public Uri StaticUrl { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("visible_in_picker")]
        public bool VisibleInPicker { get; set; }
    }

    public partial class MediaAttachment
    {
        [JsonProperty("blurhash")]
        public string Blurhash { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("pleroma")]
        public MediaAttachmentPleroma Pleroma { get; set; }

        [JsonProperty("preview_url")]
        public Uri PreviewUrl { get; set; }

        [JsonProperty("remote_url")]
        public Uri RemoteUrl { get; set; }

        [JsonProperty("text_url")]
        public Uri TextUrl { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }
    }

    public partial class MediaAttachmentPleroma
    {
        [JsonProperty("mime_type")]
        public string MimeType { get; set; }
    }

    public partial class Mention
    {
        [JsonProperty("acct")]
        public string Acct { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }

    public partial class StatusListPleroma
    {
        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public Content Content { get; set; }

        [JsonProperty("conversation_id", NullValueHandling = NullValueHandling.Ignore)]
        public long? ConversationId { get; set; }

        [JsonProperty("direct_conversation_id")]
        public object DirectConversationId { get; set; }

        [JsonProperty("emoji_reactions", NullValueHandling = NullValueHandling.Ignore)]
        public object[] EmojiReactions { get; set; }

        [JsonProperty("expires_at")]
        public object ExpiresAt { get; set; }

        [JsonProperty("in_reply_to_account_acct")]
        public string InReplyToAccountAcct { get; set; }

        [JsonProperty("local")]
        public bool Local { get; set; }

        [JsonProperty("parent_visible", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ParentVisible { get; set; }

        [JsonProperty("spoiler_text", NullValueHandling = NullValueHandling.Ignore)]
        public Content SpoilerText { get; set; }

        [JsonProperty("thread_muted", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ThreadMuted { get; set; }
    }

    public partial class Content
    {
        [JsonProperty("text/plain")]
        public string TextPlain { get; set; }
    }


    public partial class StatusList
    {
        public static StatusList[] FromJson(string json) => JsonConvert.DeserializeObject<StatusList[]>(json, FediCleaner.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this StatusList[] self) => JsonConvert.SerializeObject(self, FediCleaner.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}
