using System.Configuration;

namespace FediCleaner {
    public class Settings {
        public SettingsAccount Account {get; set;} = new SettingsAccount();
        public SettingsProcess Process {get; set;} = new SettingsProcess();
    }

    public class SettingsAccount {
        public string Site {get; set;}
        public string Username {get; set;}
    }

    public class SettingsProcess {
        public bool StatusDeletePinned {get; set;} = false;
        public int StatusWhitelistDays {get; set;} = 7;
        public string StatusInteractionFilter {get; set;} = "(d / 7) - 1";
        public bool FavoriteDelete {get; set;} = true;
        public int FavoriteAfterAge {get; set;} = 14;
    }
}