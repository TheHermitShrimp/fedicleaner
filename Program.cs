﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using NCalc;

namespace FediCleaner
{
    class Program
    {
        private static SettingsManager<Settings> ConfigurationManager;
        private static Settings Configuration;

        static void Main(string[] args)
        {
            ConfigurationManager = new SettingsManager<Settings>("appsettings.json");
            Configuration = ConfigurationManager.LoadSettings();

            AccountHandler Account = new AccountHandler(Configuration);
            if (GetSettings()) {
                ConfigurationManager.SaveSettings(Configuration);
                Console.WriteLine("Executing cleaning procedure.");
                Runner R = new Runner(Account, Configuration);
                R.HandleJobs();


            }
            Console.WriteLine("Congratulations, your profile is now spick and span.");
            Console.WriteLine("Exiting Application");
            
        }

        static bool GetSettings() {
            

            delete_pinned:
            Console.Write($"Delete pinned statuses? (Y/N) [{(Configuration.Process.StatusDeletePinned ? "Y" : "N")}]: ");
            string DeleteInput = GetInput(Configuration.Process.StatusDeletePinned ? "Y" : "N");
            if (new string[]{"Y", "N"}.Contains(DeleteInput.ToUpper())) {
                Configuration.Process.StatusDeletePinned = DeleteInput.ToUpper() == "Y";
            } else {
                Console.WriteLine("Incorrect entry.");
                goto delete_pinned;
            }

            whitelist_age:
            Console.Write($"Whitelist statuses below a certain age (in days, integer) [{Configuration.Process.StatusWhitelistDays}]: ");
            string WhitelistAge = GetInput(Configuration.Process.StatusWhitelistDays.ToString());
            int WhitelistAgeInt;
            if (Int32.TryParse(WhitelistAge, out WhitelistAgeInt)) {
                if (WhitelistAgeInt < 0) {
                    Console.WriteLine("Must be an integer above 0.");
                    goto whitelist_age;
                }
                Configuration.Process.StatusWhitelistDays = WhitelistAgeInt;
            } else {
                Console.WriteLine("Incorrect entry.");
                goto whitelist_age;
            }

            status_interactions:
            Console.WriteLine("Ignore statuses with a certain amount of interactions?");
            Console.Write($"(This can accept formulas where \"d\" is the amount of days since the status has been posted) [{Configuration.Process.StatusInteractionFilter}]: ");
            string IgnoreFormula = GetInput(Configuration.Process.StatusInteractionFilter);
            var TestExpression = new Expression(IgnoreFormula);
            TestExpression.Parameters.Add("d", 1);
            try {
                object TestExpressionValue = TestExpression.Evaluate();
                Configuration.Process.StatusInteractionFilter = IgnoreFormula;
            } catch {
                Console.WriteLine("Incorrect entry.");
                goto status_interactions;
            }

            delete_favorites:
            Console.Write($"Delete old favorites? (Y/N) [{(Configuration.Process.FavoriteDelete ? "Y" : "N")}]: ");
            string FavoriteDays = String.Empty;
            string DeleteFavorite = GetInput(Configuration.Process.FavoriteDelete ? "Y" : "N").ToUpper();
            if (new string[]{"Y", "N"}.Contains(DeleteFavorite)) {
                Configuration.Process.FavoriteDelete = DeleteFavorite == "Y";
                if (!Configuration.Process.FavoriteDelete) {
                    goto end;
                }
            } else {
                Console.WriteLine("Incorrect entry.");
                goto delete_favorites;
            }

            favorites_age:
            Console.Write($"Delete favorites after how many days [{Configuration.Process.FavoriteAfterAge}]: ");
            FavoriteDays = GetInput(Configuration.Process.FavoriteAfterAge.ToString());
            int FavoriteDaysInt;
            if (Int32.TryParse(FavoriteDays, out FavoriteDaysInt)) {
                Configuration.Process.FavoriteAfterAge = FavoriteDaysInt;
            } else {
                Console.WriteLine("Incorrect entry.");
                goto favorites_age;
            }

            end:
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("You chosen settings:");
            Console.WriteLine("Delete pinned statuses: " + DeleteInput);
            Console.WriteLine("Whitelist statuses below a certain age: " + WhitelistAge);
            Console.WriteLine("Ignore statuses with a certain amount of interactions (favorites, replies, boosts): " + IgnoreFormula);
            Console.WriteLine("Delete old favorites: " + DeleteFavorite);
            if (Configuration.Process.FavoriteDelete) {
                Console.WriteLine("Delete favorites after how many days: " + FavoriteDays);
            }
            Console.Write("Is this correct? [Y/N]: ");
            string confirmString = GetInput(null).ToUpper();
            if (new string[]{"Y", "N"}.Contains(confirmString)) {
                return (confirmString == "Y");
            } else {
                Console.WriteLine("Incorrect entry.");
                goto delete_favorites;
            }
        }

        private static string GetInput(string Default) {
            test_loop:
            string Input = Console.ReadLine();
            Input = String.IsNullOrEmpty(Input) ? Default: Input;
            if (String.IsNullOrEmpty(Input)) {
                Console.Write("This value cannot be empty. Please enter a value: ");
                goto test_loop;
            }
            return Input;
        }
    }

    /*
        SETTINGS
        Delete pinned statuses?
        White list statuses below a certain age.
        Keep statuses with a certain amount of interactions?
        Delete old favorites?
        -After how many days?
    */

    public class Runner {

        private AccountHandler Account;
        private Settings Configuration;
        public Runner(AccountHandler _Account, Settings _Configuration) {
            this.Account = _Account;
            this.Configuration = _Configuration;
        }
        public void HandleJobs() {
            
            List<StatusList> DeletedStatuses = new List<StatusList>();
            string max_id = null;
            Expression DayCalc = new Expression(Configuration.Process.StatusInteractionFilter);
            DayCalc.Parameters.Add("d", 0);
            
            while (true) {
                List<StatusList> Statuses = Account.GetStatuses(max_id);
                if (Statuses.Count == 0) {
                    break;
                }

                max_id = Statuses.Last().Id;

                for (int i = 0; i < Statuses.Count; i++) {
                    StatusList S = Statuses[i];
                    int Days = Convert.ToInt32((DateTime.Now - S.CreatedAt).TotalDays);
                    DayCalc.Parameters["d"] = Days;
                    if (
                        (Days <= Configuration.Process.StatusWhitelistDays) ||
                        (S.Pinned && !Configuration.Process.StatusDeletePinned) ||
                        (S.FavouritesCount + S.ReblogsCount + S.RepliesCount >= Convert.ToDouble(DayCalc.Evaluate()))

                    ){
                        Statuses.RemoveAt(i);
                        i--;
                    } else {
                        if (S.Reblog == null) {
                            Account.DeleteStatus(S.Id);
                        } else {
                            Account.DeleteReblog(S.Reblog.Id);
                        }
                        
                    }
                }
                Console.WriteLine($"Deleted {Statuses.Count} on this pass.");
                DeletedStatuses.AddRange(Statuses);
            }

            Console.WriteLine("Deleted " + DeletedStatuses.Count + " statuses total.");

            /// Favorites
            if (Configuration.Process.FavoriteDelete) {
                DateTimeOffset CullTime = new DateTimeOffset(DateTime.Now.AddDays(Configuration.Process.FavoriteAfterAge));
                List<StatusList> DeletedFavorites = new List<StatusList>();
                max_id = null;
                while(true) {
                    List<StatusList> Favs = Account.GetFavorites(max_id);
                    if (Favs.Count == 0) {
                        break;
                    }
                    max_id = Favs.Last().Id;
                    for (int i = 0; i < Favs.Count; i++) {
                        if (Favs[i].CreatedAt < CullTime) {
                            Account.DeleteFavorite(Favs[i].Id);
                        } else {
                            Favs.RemoveAt(i);
                            i--;
                        }
                    }
                    DeletedFavorites.AddRange(Favs);
                }

                

                Console.WriteLine("Deleted " + DeletedFavorites.Count + " favorites.");
            }
        }
    }
}
