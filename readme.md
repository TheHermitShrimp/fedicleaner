# About the project
I've always had a tendency to keep my social media profiles very clean and never particularly liked building up more and more storage for no other reason than a post count.  I created this tool so that I can keep my posts and favorites pruned based on when they were posted and how particularly interesting they turned out to be (interactions).

# Features
- Ignoring pinned statuses
- Whitelisting statuses below a certin age
- Ignoring statuses with a certain amount of interactions
  - This can scale off of a formula where "d" is the number of days since posted.
- Deleting favorites after a certain number of days

**DON'T WORRY. YOU WILL HAVE TO CONFIRM BEFORE IT STARTS RIPPING AND TEARING.**

# Building
.NET 5.0 project

Build and run.
